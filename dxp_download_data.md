Here, you will find an overview of the open source informmation of this product. The detailed information can be found within the repository at [GitLab](https://gitlab.com/ourplant.net/products/s1-0007-ourplant_d1).

| document | download options |
|:-------- | ----------------:|
|operating manual           |[de](https://gitlab.com/ourplant.net/products/s1-0007-ourplant_d1/-/raw/main/01_operating_manual/S1-0007_C_Betriebsanleitung.pdf) / [en](https://gitlab.com/ourplant.net/products/s1-0007-ourplant_d1/-/raw/main/01_operating_manual/S1-0007_C_Operating%20Manual.pdf)|
|assembly drawing           |[de](https://gitlab.com/ourplant.net/products/s1-0007-ourplant_d1/-/raw/main/02_assembly_drawing/s1-0007_01_A_ZNB_ourplant_d1%20mit%20UG.pdf)|
|circuit diagram            |[de](https://gitlab.com/ourplant.net/products/s1-0007-ourplant_d1/-/raw/main/03_circuit_diagram/S1-0007_OurPlant_Desktop_D1.pdf)|
|maintenance instructions   ||
|spare parts                |[de](https://gitlab.com/ourplant.net/products/s1-0007-ourplant_d1/-/raw/main/05_spare_parts/S1-0007_B_EVL_OurPlant-D1.pdf), [en](https://gitlab.com/ourplant.net/products/s1-0007-ourplant_d1/-/raw/main/05_spare_parts/S1-0007_B_EVL_OurPlant-D1_engl.pdf)|

