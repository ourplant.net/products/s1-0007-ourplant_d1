Beginnen Sie mit der professionellen Automatisierung Ihrer Ideen bereits im Labor. Die OurPlant D1 ist die Basis für den nahtlosen Übergang Ihrer Produkte in die Serienfertigung.
Die Anlage stellt ein intelligentes Entwicklertool mit dem Niveau einer Produktionsmaschine dar.

Wie alle Anlagen der OurPlant-Familie basiert die Desktopanlage auf einem modularen Konzept. Dank einer Vielzahl von Bearbeitungsmodulen ist die Plattform in der Lage, unterschiedlichste Applikationen zur Mikrobestückung sowie -dosierung abzubilden. Das modulare und erweiterbare Konzept garantiert die Anpassungsfähigkeit der OurPlant D1 an zukünftige Produkte und Prozessanforderungen. Module und Programme können später einfach auf alle größeren Maschinen der OurPlant-Familie transferiert werden. In der Mikromontageanlage werden die Module auf der Lochrastergrundplatte frei positioniert und die Bearbeitungsköpfe auf dem offenen Achsportal per Plug&Play aufgenommen. Die Bedienung und Steuerung erfolgt mittels Laptop.

**Vorteile:**
– Ideal für Prototypen- und Kleinserienfertigung
– Geringe Rüstaufwände durch echte Plug & Play –Fähigkeit

**Technische Daten:**
– 2-Achsportalsystem (X, Y)
– Bearbeitungsköpfe mit integrierter Z-Achse
– Bediener-Laptop
– Software und Steuerung
– Interface mit 10 elektrischen Anschlüssen (5x CAN, 5x Ethernet) für die Aufnahme von Bearbeitungsmodulen bis zu einer Gesamtbreite von 150 mm (in der Standardausführung), durch eine Adapterplatte optional erweiterbar bis 250 mm