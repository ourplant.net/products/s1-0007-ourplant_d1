The desktop machine OurPlant D1 is the basis for the seamless transition of your products into series production.
The system represents an intelligent developer tool with the quality of a production machine. Like all systems of the OurPlant family, the desktop system is based on a modular concept. Thanks to a large number of processing modules, the platform is able to reproduce a wide variety of micro assembly and dispensing applications. The modular and extensible concept guarantees the adaptability of OurPlant D1 to future products and process requirements. Later modules and programs can be easily transferred to all larger machines in the OurPlant family.

In this micro assembly machine all modules are freely positioned on the perforated grid base plate and the processing heads are installed on the open axis portal via Plug & Play. The operation and control is done by means of a laptop.

**Advantages:**
– Ideal for prototype and small batch production
– Low set-up effort thanks to real plug & play capability

**Technical specifications:**
– 2-axis gantry system (X, Y)
– Working heads with integrated Z-axis
– Laptop
– Software and control